extends Node


var i = 1
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("input_next"):
		# if no level past the current one is detected
		if ResourceLoader.exists("res://scenes/Level" + str(i + 1) + ".tscn") == false: 
			i = 1 # reset to first level (line 19)
		else:
			i += 1 # go to next level (line 19)
		# changes scene to level[i].tscn
		get_tree().change_scene_to_file("res://scenes/Level" + str(i) + ".tscn")
