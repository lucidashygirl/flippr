# Flippr

## About

This is my first game made in godot, it's got limited functionality for now and kinda
sucks, but whatever. I hope you have fun.

## Installation

Executables for both Linux and Windows are in the export directory, the rest is just the project files.

## How to play

There are no in-game text prompts so the controls will be listed here

- left/right to move in those directions (wow!)
- z to flip gravity
- x to restart level
- s to go to next level

The game mechanics are pretty simple, but I'll explain anyways

- blue tiles are boring and do nothing
- red tiles kill you
- purple tiles flip your gravity upon contact
- yellow tiles disable your gravity flip

