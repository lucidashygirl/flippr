#!/bin/sh
echo -ne '\033c\033]0;flippr\a'
base_path="$(dirname "$(realpath "$0")")"
"$base_path/flippr.x86_64" "$@"
